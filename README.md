# TezGraph sample client

TezGraph sample client allows Application Developers to run and test basic subscriptions to TezGraph pubsub via web sockets

More TezGraph details at [https://tezgraph.com/quickstart](https://tezgraph.com/quickstart).

## Running Locally for Development

Prerequisites:
1. NodeJS (latest version 14) (https://nodejs.org/en/download/)
1. Run `npm install`
1. Specify the desired TezGraph endpoint (edit `index.ts` tezgraphUrl constant)
1. Specify the desired subscriptions from predefined to subscribe to (edit `app.ts` subscribeTo constant)
1. Run `npm run dev`
1. Watch console output for incoming notifications
