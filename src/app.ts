import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { WebSocketLink } from 'apollo-link-ws';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import ws from 'ws';

import { SubscriptionsCreator, SubscriptionType } from './subscriptions-creator';

export class App {
    private wsClient!: SubscriptionClient;
    private apolloClient!: ApolloClient<NormalizedCacheObject>;
    private isStopping: boolean = false;

    constructor(private tezgraphUrl: string) {}

    async start(): Promise<void> {
        await this.initializeSubscriptions();

        ['SIGINT', 'SIGTERM'].forEach(s => process.on(s, () => this.stop()));

        console.log('Sample client has started');
        console.log('Waiting for notifications');
    }

    stop(): void {
        if (this.isStopping) {
            return;
        }

        this.isStopping = true;

        this.apolloClient.stop();
        this.wsClient.unsubscribeAll();

        console.log('Sample client has stopped');
    }

    private async initializeSubscriptions() : Promise<void> {
        this.wsClient = new SubscriptionClient(
            this.tezgraphUrl,
            { reconnect: true },
            ws,
        );

        this.apolloClient = new ApolloClient({
            link: new WebSocketLink(this.wsClient),
            cache: new InMemoryCache(),
        });
        
        const processFunc = (data: { name: string, notification: unknown }) => {
            this.processNotification(data);
        };

        const subscribeTo = [SubscriptionType.transaction];
        // const subscribeTo = [SubscriptionType.endorsement_with_mempool];
        // const subscribeTo = [SubscriptionType.block_with_endorsements];
        // const subscribeTo = [SubscriptionType.transaction, SubscriptionType.endorsement_with_mempool, SubscriptionType.block_with_endorsements];

        const subscriptionsCreator = new SubscriptionsCreator(this.apolloClient, processFunc);
        const subscriptions = subscriptionsCreator.createSubscriptions(subscribeTo);

        subscriptions.forEach(s => console.log(`Created '${s.name}' subscription`));
    }

    private processNotification(data: { name: string, notification: any }) {
        console.log(JSON.stringify(data));
    }
}
