import { NormalizedCacheObject } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { DocumentNode } from 'graphql';

import { blockAddedWithEndorsements } from './gql/block_added_with_endorsements';
import { transactionAdded } from './gql/transaction-added';
import { endorsementAdded } from './gql/endorsement-added';

export enum SubscriptionType {
    transaction = 'transaction',
    endorsement_with_mempool = 'endorsement_with_mempool',
    block_with_endorsements = 'block_with_endorsements',
}

export class SubscriptionsCreator {
    constructor(
        private readonly apolloClient: ApolloClient<NormalizedCacheObject>,
        private readonly processFunc: (notification: { name: string, notification: unknown }) => void,
    ) {}

    createSubscriptions(subscriptionTypes: SubscriptionType[]) : any[] {
        const subscriptions: any[] = [];

        subscriptionTypes.forEach(t => {
            if (t === SubscriptionType.transaction) {
                const transactionFilter = {
                    includeMempool: false,
                    source: {
                        notEqualTo: 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
                        notIn: ['tz1iHtecBqvddFzQ9Jg9tEnpH5CJSPYv4Ww7', 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93'],
                    },
                    destination: { 
                        notIn: ['tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93', 'tz1iHtecBqvddFzQ9Jg9tEnpH5CJSPYv4Ww7'] 
                    },
                    status: { 
                        equalTo: 'applied', 
                        notIn: ['backtracked', 'skipped'] 
                    },
                    amount: { 
                        greaterThan: 100000 
                    },
                };
                subscriptions.push(this.createSubscription('transaction', transactionAdded, transactionFilter));
            } 
            else if (t === SubscriptionType.endorsement_with_mempool) {
                const endorsementFilter = {
                    includeMempool: true,
                };
                subscriptions.push(this.createSubscription('endorsement_with_mempool', endorsementAdded, endorsementFilter));
            } 
            else if (t === SubscriptionType.block_with_endorsements) {
                subscriptions.push(this.createSubscription('block_with_endorsements', blockAddedWithEndorsements));
            } 
            else {
                console.log('Unsupported subscription ' + t);
            }
        });

        return subscriptions;
    }

    private createSubscription(name: string, query: DocumentNode, filter?: Record<string, unknown>): any {
        const subscription = this.apolloClient
            .subscribe({
                query,
                variables: filter,
            })
            .subscribe({
                next: notification => this.processFunc({ name, notification }),
            });

        return { name, subscription };
    }
}