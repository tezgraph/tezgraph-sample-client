import { App } from './app';

const tezgraphUrl = 'wss://tezgraph-staging.tezoslive.io/graphql';

async function bootstrap(): Promise<void> {
    await new App(tezgraphUrl).start();
}

void bootstrap();
