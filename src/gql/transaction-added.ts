import { gql } from 'apollo-server';

export const transactionAdded = gql`
subscription (
    # Common parameters.
    $includeMempool: Boolean,
    $replayFromBlockLevel: Int,
    $hash: NullableOperationHashFilter,
    $protocol: ProtocolHashFilter,
    $branch: BlockHashFilter,

    # Specific parameters.
    $source: AddressFilter,
    $destination: AddressFilter,
    $status: NullableOperationResultStatusFilter
) {
    transactionAdded(
        # Common parameters.
        includeMempool: $includeMempool,
        replayFromBlockLevel: $replayFromBlockLevel,
        filter: {
            hash: $hash
            protocol: $protocol,
            branch: $branch,

            # Specific parameters.
            source: $source,
            destination: $destination,
            status: $status
        }
    ) {
        info {
            hash,
            signature
        },
        kind,
        source,
        fee,
        counter,
        gas_limit,
        storage_limit,
        amount,
        destination,
        parameters {
            entrypoint,
            value
        },
        block {
          hash,
          header {
            level,
            timestamp
          }
        },
        metadata {
            balance_updates {
                kind,
                category,
                contract,
                delegate,
                cycle,
                change
            },
            internal_operation_results {
                kind,
                source,
                nonce,
                amount,
                destination,
                balance
            },
            operation_result {
                status,
                consumed_gas,
                errors {
                    kind,
                    id
                },
                storage,
                balance_updates {
                    kind,
                    category,
                    contract,
                    delegate,
                    cycle,
                    change
                },
                originated_contracts,
                storage_size,
                paid_storage_size_diff,
                allocated_destination_contract
            }
        }
    }
}
`;
