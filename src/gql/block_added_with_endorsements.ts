import { gql } from 'apollo-server';

export const blockAddedWithEndorsements = gql`
subscription {
  blockAdded {
    hash,
    header {
      level,
      timestamp
    },
    endorsements {
      info {
    		hash,
    		signature
  		},
      kind,
      level,
      block {
	      hash,
        header {
  	      level,
          timestamp
        }
      },
      metadata {
        balance_updates {
          kind,
          category,
          contract,
          delegate,
          cycle,
          change
        },
        delegate,
        slots
      }
    }
  }
}`;