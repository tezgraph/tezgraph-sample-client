FROM node:12.19.0-alpine3.12

RUN apk add --no-cache git
WORKDIR /usr/src/app
ADD . /usr/src/app
RUN npm ci
CMD ["npm", "run", "dev"]
